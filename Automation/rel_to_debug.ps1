# -------------------------------------------------------------------------------
# https://docs.microsoft.com/en-us/rest/api/power-bi/
# https://docs.microsoft.com/en-us/powershell/power-bi/overview?view=powerbi-ps
# If you want to use the authenticated session outside of PowerShell, get the access 
# token by using: Get-PowerBIAccessToken -AsString
# GitHub commands link: https://github.com/microsoft/powerbi-powershell
# -------------------------------------------------------------------------------

# Import libraries 
Import-Module MicrosoftPowerBIMgmt
Import-Module MicrosoftPowerBIMgmt.Profile

$instructions = Get-Content -Path .\powershell\deploy_instructions.json | Out-String | ConvertFrom-Json;

$password = $instructions.password | ConvertTo-SecureString -AsPlainText -Force ;
$username = $instructions.username ;
$credential = New-Object System.Management.Automation.PSCredential($username, $password) ;

# Connect to Power Bi Service
Connect-PowerBIServiceAccount -Credential $credential ;