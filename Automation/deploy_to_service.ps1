﻿
# -------------------------------------------------------------------------------
# https://docs.microsoft.com/en-us/rest/api/power-bi/
# https://docs.microsoft.com/en-us/powershell/power-bi/overview?view=powerbi-ps
# If you want to use the authenticated session outside of PowerShell, get the access 
# token by using: Get-PowerBIAccessToken -AsString
# GitHub commands link: https://github.com/microsoft/powerbi-powershell
# -------------------------------------------------------------------------------

# Import libraries 
Import-Module MicrosoftPowerBIMgmt
Import-Module MicrosoftPowerBIMgmt.Profile

# Setting variables
$password = "<fill in the password>" | ConvertTo-SecureString -AsPlainText -Force
$username = "<fill in the username>"
$credential = New-Object System.Management.Automation.PSCredential($username, $password)
$sourceReportGroupId = "f599fc18-2e29-4abf-947f-63f8f28d7103" # similar to workspace id
$sourceDataset = "b9a228a3-4fd2-46cd-91c5-870da0c916e1" # id from pbi service
$targetDataset = "156765ce-d7eb-49aa-b0f8-6df7226f411a" # id from pbi service


# Connect to Power Bi Service
Connect-PowerBIServiceAccount -Credential $credential


# List reports with older dataset
$uri = "https://api.powerbi.com/v1.0/myorg/groups/$sourceReportGroupId/reports"
$response = Invoke-RestMethod -Uri $uri -Headers $authHeader -Method Get -Verbose 
$fromlist = $response.value | Where-Object {$_.datasetId -eq $sourceDataset}
$listReports = $fromlist.id # list with all reports ids which have the previous dataset

## Building REST call

# Body
$postParams = @{
    "datasetId" = "$targetDataset" 
}

$jsonPostBody = $postParams | ConvertTo-JSON

# Header
$authHeader = @{
   'Content-Type'='application/json'
   'Authorization'= Get-PowerBIAccessToken -AsString
}

# API call
foreach($report in $listReports)
{
    # Make the request
    $uri = "https://api.powerbi.com/v1.0/myorg/groups/$sourceReportGroupId/reports/$report/Rebind"
    Invoke-RestMethod -Uri $uri -Headers $authHeader -Method POST -Body $jsonPostBody -Verbose
}


# Closes connection
Disconnect-PowerBIServiceAccount 