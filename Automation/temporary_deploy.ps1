﻿
# -------------------------------------------------------------------------------
# https://docs.microsoft.com/en-us/rest/api/power-bi/
# https://docs.microsoft.com/en-us/powershell/power-bi/overview?view=powerbi-ps
# If you want to use the authenticated session outside of PowerShell, get the access 
# token by using: Get-PowerBIAccessToken -AsString
# GitHub commands link: https://github.com/microsoft/powerbi-powershell
# -------------------------------------------------------------------------------

# Import libraries 
Import-Module MicrosoftPowerBIMgmt
Import-Module MicrosoftPowerBIMgmt.Profile

$instructions = Get-Content -Path .\powershell\deploy_instructions.json | Out-String | ConvertFrom-Json;

$password = $instructions.password | ConvertTo-SecureString -AsPlainText -Force ;
$username = $instructions.username ;
$credential = New-Object System.Management.Automation.PSCredential($username, $password) ;
$powerbiPath = "C:\Users\Maktu\MsC_projetos\Upwork\Staykeepers\Backup\All - LIVE (21092021).pbix" ;
$newWorkspaceName = $instructions.'new workspace name' ;
$sourceWorkspaceName = $instructions.'source workspace' ;

# Connect to Power Bi Service
Connect-PowerBIServiceAccount -Credential $credential ;

$sourceReportGroupId = (Get-PowerBIWorkspace -Name $sourceWorkspaceName).Id ;
$sourceDataset = "b9a228a3-4fd2-46cd-91c5-870da0c916e1"; # id from pbi service
#$sourceDataset = (Get-PowerBIDataset -WorkspaceId $sourceReportGroupId -Name $instructions.'source dataset name').Id

Write-Output "Successfully connected to powerbi service";

# Build rest headers

$authHeader = @{
    'Content-Type'='application/json'
    'Authorization'= Get-PowerBIAccessToken -AsString
}

# Creates a new workspace for temporary evaluation

if( $null -eq (Get-PowerBIWorkspace -Name $newWorkspaceName ) ) # if false workspace exists
{
    
    New-PowerBIWorkspace -Name $newWorkspaceName
    $newWorkspaceId = (Get-PowerBIWorkspace -Name $newWorkspaceName).Id

} else {
    
    $newWorkspaceId = (Get-PowerBIWorkspace -Name $newWorkspaceName).Id

};

Write-Output "Successfully created new temporary workspace & with ID:$newWorkspaceId";

# Uploads new report into temporary workspace
# TODO: needs to test if the dataset exists otherwise needs to replace

#New-PowerBIReport -Path $powerbiPath -Name $instructions.'new dataset name' -Workspace ( Get-PowerBIWorkspace -Name $newWorkspaceName ) -ConflictAction Overwrite 

#Copy-PowerBIReport -Name "All Live Backup 21092021" -Id (Get-PowerBIReport -Name $instructions.'source dataset name' -WorkspaceId $sourceReportGroupId).Id -WorkspaceId $sourceReportGroupId -TargetWorkspaceId $newWorkspaceId

$newDatasetId = (Get-PowerBIDataset -WorkspaceId $newWorkspaceId -Name "All - LIVE (21092021)").Id;

$newDatasetId

Write-Output "Deploy successful of new dataset with Id: $newDatasetId";

# List reports with older dataset

$uri = "https://api.powerbi.com/v1.0/myorg/groups/$sourceReportGroupId/reports";
$response = Invoke-RestMethod -Uri $uri -Headers $authHeader -Method Get -Verbose ;
$response ;
$fromlist = $response.value | Where-Object {$_.datasetId -eq $sourceDataset} ;
$listReports = $fromlist.id ;

Write-Output "Found the following reports to be copied $ListReports";

# Copies reports into new workspace and change dataset
foreach($report in $fromlist)
{
    Copy-PowerBIReport -Name $report.name -Id $report.id -WorkspaceId $sourceReportGroupId -TargetWorkspaceId $newWorkspaceId -TargetDatasetId $newDatasetId
};

Write-Output "Successfully cloned all reports and redirected dataset. Everything ready for review & QA."

# Closes connection
Disconnect-PowerBIServiceAccount 