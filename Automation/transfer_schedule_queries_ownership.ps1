# -------------------------------------------------------------------------------
# Name:
# Purpose:
# Version:
# Last change:
# Arguments:
# 
# Requires Gcloud and author correctly configured on machine or have 
# downloaded service file json
#------------------------------------------------------------------------------

$sqQueries =  bq ls --format=prettyjson --transfer_config --transfer_location=europe-west2 --project_id=xero-245302;

$list = $sqQueries | ConvertFrom-Json;
$query_name = $list.name 

foreach($query in $query_name) 
{
    bq update `
    --transfer_config `
    --update_credentials `
    --service_account_name=gsheetswithpandas-339@xero-245302.iam.gserviceaccount.com `
    $query
}

Write-Output "Service account successfully took ownership of scheduled queries"

