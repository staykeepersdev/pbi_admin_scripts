# ALL LIVE dataset
## What is this file used for:

This file should contain a summary of relevant changes on this report as well as important information to understand both the model and the visuals constructed. Any reference to measures should be included here as well as any reference necessary to rebuild the report.

## Logs

----
#### v 1.0.10
```
comments:
- add new refactored metric for correct available nights

>>> +++ measure
Available_nights_refactor = 
CALCULATE(
    	[Available_nights],
    	CROSSFILTER(CrossJ_HF[cfUNIQUE NUMBER], ID_REG[property_unique_number],None)
	)

>>> +++ measure
insurance_refactor = [gross_profir_report] * 0.015

>>> +++ measure
gross_profir_report = 
VAR __directCost =  [Agoda_commission] + [Airbnb_commission] + [Booking_commission] +[CTrip_commission]+ [Direct_commission] + [Expedia_commission] + [HomeAway_commission] + [Hostelworld_commission] + [Spotahome_commission] + [Tripadvisor_commission] + [TUI Villas_commission] + [c day transaction fee] + [Disputes]+[Negative Adjustments]
VAR __grossProfit = [Client_Revenue] + __directCost
VAR __managFee = [Manag. Fee_%] * __grossProfit
VAR __vat = [VAT_%] * __grossProfit
VAR __corrected = __grossProfit - [Negative Adjustments] + [negative adjustments report] + [other_revenue] - __managFee - __vat
VAR __insurance = 0.015 *__corrected
VAR __result = __corrected - __insurance
RETURN
    __corrected
    
>>> +++ measure   
Gross_profit_insurance_% = 
VAR __directCost =  [Agoda_commission] + [Airbnb_commission] + [Booking_commission] +[CTrip_commission]+ [Direct_commission] + [Expedia_commission] + [HomeAway_commission] + [Hostelworld_commission] + [Spotahome_commission] + [Tripadvisor_commission] + [TUI Villas_commission] + [c day transaction fee] + [Disputes]+[Negative Adjustments]
VAR __grossProfit = [Client_Revenue] + __directCost
VAR __managFee = [Manag. Fee_%] * __grossProfit
VAR __vat = [VAT_%] * __grossProfit
VAR __corrected = __grossProfit - [Negative Adjustments] + [negative adjustments report] + [other_revenue] - __managFee - __vat
VAR __insurance_neighborhood =  CALCULATE(
    [Insurance_of_Landlord_Acc], 
    --ALL(CrossJ_HF),
    ID_REG[neighborhood] = VALUES(CrossJ_HF[r_neighbourhood])
    )
VAR __insurance = IF(__insurance_neighborhood = BLANK(),0, 0.015 *__corrected * (1 - [Manag. Fee_%]) * (1-[VAT_%]) ) 
VAR __result = __corrected - __insurance
RETURN
    __result
    
```

----
#### v 1.0.9
```
comments:
- Manually added vat 0% for non uk since a automated procedure wasn't found

>>> --- measure
IF_VAT = 
/*IF(
    CrossJ_HF[Date]>=DATE(2020,7,15) && CrossJ_HF[Date]<=DATE(2021,9,30), CrossJ_HF[cVAT_5%], CrossJ_HF[cVAT20%]
    )*/
SWITCH(
    TRUE(),
    CrossJ_HF[Date]>=DATE(2020,7,15) && CrossJ_HF[Date]<=DATE(2021,9,30) && CrossJ_HF[r_neighbourhood] = "Lucia Foster", 0.01,
    CrossJ_HF[Date]>=DATE(2020,7,15) && CrossJ_HF[Date]<=DATE(2021,9,30) , CrossJ_HF[cVAT_5%],
    CrossJ_HF[Date]>DATE(2021,9,30) && CrossJ_HF[Date]<=DATE(2022,3,31) , CrossJ_HF[cVAT12,5%],
    CrossJ_HF[Date] > DATE(2022,3,31), CrossJ_HF[cVAT20%],    
    0
)
>>> +++ measure
IF_VAT = 
/*IF(
    CrossJ_HF[Date]>=DATE(2020,7,15) && CrossJ_HF[Date]<=DATE(2021,9,30), CrossJ_HF[cVAT_5%], CrossJ_HF[cVAT20%]
    )*/
SWITCH(
    TRUE(),
    CrossJ_HF[Date]>=DATE(2020,7,15) && CrossJ_HF[Date]<=DATE(2021,9,30) && CrossJ_HF[r_neighbourhood] = "Lucia Foster", 0.01,
    CrossJ_HF[r_neighbourhood] = "Livensa",0,
    CrossJ_HF[r_neighbourhood] = "Our Lady Of Fatima Residence",0,
    CrossJ_HF[r_neighbourhood] = "Point Campus",0,
    CrossJ_HF[r_neighbourhood] = "Liv Student",0,
    CrossJ_HF[r_neighbourhood] = "Westwood House",0,
    CrossJ_HF[r_neighbourhood] = "Highfield House",0,
    CrossJ_HF[r_neighbourhood] = "Dubailand",0,
    CrossJ_HF[Date]>=DATE(2020,7,15) && CrossJ_HF[Date]<=DATE(2021,9,30) , CrossJ_HF[cVAT_5%],
    CrossJ_HF[Date]>DATE(2021,9,30) && CrossJ_HF[Date]<=DATE(2022,3,31) , CrossJ_HF[cVAT12,5%],
    CrossJ_HF[Date] > DATE(2022,3,31), CrossJ_HF[cVAT20%],    
    0
)
```

----
#### v 1.0.8

```
"""
comments:
- VAT only aplicable to United Kingdon Vat. Since no information exists regarding landlord country, we used currency as proxy
- Change column containing payment information so it can track manual changes jn bigquery (see bigquery view code)
- Add new revenue source which automatically considers all non specific sources 
"""

>> --- power query
changed HF_res to read paid information from table paid_part_manualCorrection instead aof paid_part

>> +++ measure
Manag. Fee_% = 
SWITCH(
    TRUE(),
    MAX( CrossJ_HF[r_neighbourhood] ) = "Lucia Foster" && MAX(CrossJ_HF[Date]) >= DATE(2021,8,1) && MAX(CrossJ_HF[Date]) <= DATE(2021,9,30),  0.2,
    MAX(CrossJ_HF[currency]) <> "GBP", AVERAGEX(ID_REG, ID_REG[cCOMMISSION_%] )* 1.2 ,
    AVERAGEX( ID_REG, ID_REG[cCOMMISSION_%] )
)

>> --- measure
Manag. Fee_% = 
SWITCH(
    TRUE(),
    MAX( CrossJ_HF[r_neighbourhood] ) = "Lucia Foster" && MAX(CrossJ_HF[Date]) >= DATE(2021,8,1) && MAX(CrossJ_HF[Date]) <= DATE(2021,9,30),  0.2,
    MAX(CrossJ_HF[currency]) <> "GBP", AVERAGEX(ID_REG, ID_REG[cCOMMISSION_%] )* 1.2 ,
    AVERAGEX( ID_REG, ID_REG[cCOMMISSION_%] )
)

>> +++ measure
Manag. Fee_% = 
SWITCH(
    TRUE(),
    MAX( CrossJ_HF[r_neighbourhood] ) = "Lucia Foster" && MAX(CrossJ_HF[Date]) >= DATE(2021,8,1) && MAX(CrossJ_HF[Date]) <= DATE(2021,9,30),  0.2,
    MAX(CrossJ_HF[r_neighbourhood] ) = "Our Lady of Fatima Residence", 0.1,
    MAX(CrossJ_HF[r_neighbourhood] ) = "Liv Student", 0.2,
    MAX(CrossJ_HF[r_neighbourhood] ) = "Dubailand", 0.1,
    MAX(CrossJ_HF[currency]) <> "GBP", AVERAGEX(ID_REG, ID_REG[cCOMMISSION_%] ) ,
    AVERAGEX( ID_REG, ID_REG[cCOMMISSION_%] )
)

>> --- measure

Manag. Fee_% = 
IF(
    MAX(CrossJ_HF[r_neighbourhood]) = "Lucia Foster" && 
        MAX(CrossJ_HF[Date]) >= DATE(2021,8,1) &&
        MAX(CrossJ_HF[Date]) <= DATE(2021,9,30), 
    0.2,
    AVERAGEX(ID_REG,
            ID_REG[cCOMMISSION_%]
    )
) 

>> +++ measure
Other_channels_Revenue = 
[Agoda_Revenue]+ [CTrip_Revenue]+[Expedia_Revenue] + [HomeAway_Revenue] + [Hostelworld_Revenue]+[Spotahome_Revenue]+[Tripadvisor_Revenue]+[TUI Villas_Revenue] + [other_revenue]

>> --- measure
Other_channels_Revenue = 
[Agoda_Revenue]+ [CTrip_Revenue]+[Expedia_Revenue] + [HomeAway_Revenue] + [Hostelworld_Revenue]+[Spotahome_Revenue]+[Tripadvisor_Revenue]+[TUI Villas_Revenue]

>> +++ measure
other_revenue = 
CALCULATE (
		    SUM ( CrossJ_HF[c_day revenue_updated] ),
		    NOT( CrossJ_HF[Source_C] ) IN {"Agoda", "Airbnb", "Booking", "CTrip", 
		    	"Direct", "Expedia","HomeAway", "Hostelworld", "Spotahome", "Tripadvisor", "TUI Villas"}
		)

>>> +++ measure
Other_income = 
CALCULATE(
    CALCULATE ( SUM ( ca_invoices[c lineitem_inv_bill] ) ,
        ca_invoices, ca_invoices[lineitems_accountcode] = 122
),CROSSFILTER(CrossJ_HF[cfUNIQUE NUMBER], ID_REG[property_unique_number], OneWay_RightFiltersLeft))

>>> --- measure
Other_income = 
CALCULATE ( SUM ( ca_invoices[c lineitem_inv_bill] ) ,
     ca_invoices, ca_invoices[lineitems_accountcode] = 122
)

>>> +++ measure
Client_Revenue = 
[Agoda_Revenue]+[Airbnb_Revenue] + [Booking_Revenue] +[CTrip_Revenue]+[Direct_Revenue]+ [Expedia_Revenue] +
[HomeAway_Revenue] + [Hostelworld_Revenue]+[Spotahome_Revenue]+[Tripadvisor_Revenue]+[TUI Villas_Revenue] + 
[Won Disputes]+[Positive Adjustments] + [other_revenue]

>>> --- measure
Client_Revenue = 
[Agoda_Revenue]+[Airbnb_Revenue] + [Booking_Revenue] +[CTrip_Revenue]+[Direct_Revenue]+ [Expedia_Revenue] + 
[HomeAway_Revenue] + [Hostelworld_Revenue]+[Spotahome_Revenue]+[Tripadvisor_Revenue]+[TUI Villas_Revenue] + 
[Won Disputes]+[Positive Adjustments]

>>> +++ measure
other_commission = 
CALCULATE (
		     - SUM ( CrossJ_HF[c_day channel_commission] ),
		    NOT( CrossJ_HF[Source_C] ) IN {"Agoda", "Airbnb", "Booking", "CTrip", 
		    	"Direct", "Expedia","HomeAway", "Hostelworld", "Spotahome", "Tripadvisor", "TUI Villas"}
		)

>>> +++ measure
Direct_Cost = 
+ [Agoda_commission] + [Airbnb_commission] + [Booking_commission] +[CTrip_commission]+ [Direct_commission] + 
[Expedia_commission] + [HomeAway_commission] + [Hostelworld_commission] + [Spotahome_commission] + 
[Tripadvisor_commission] + [TUI Villas_commission] + [c day transaction fee] + [Disputes] + 
[Negative Adjustments]+[Insurance_of_Landlord_Acc] + [other_commission]

```

----
#### v 1.0.7

```
"""
comment: 
- % target was not taking in consideration exchange rate
- cleaning refund was being filtered against reservations although they are unrelated
"""

>> +++ measure
%_Target_Achieved_refactor = [Gross_Rent_GBP] / [Rev_Target_refactor]

>> --- measure
%_Target_Achieved_refactor = [Gross_Rent] / [Rev_Target_refactor]

>> +++ measure
Cleaning fee - refund to the landlord = 
CALCULATE(
    CALCULATE ( SUM ( ca_invoices[c lineitem_inv_bill] ) ,
        ca_invoices, ca_invoices[lineitems_accountcode] = 121),     
        CROSSFILTER(CrossJ_HF[cfUNIQUE NUMBER], ID_REG[property_unique_number], OneWay_RightFiltersLeft)
    )
    
>> --- measure
Cleaning fee - refund to the landlord = 
CALCULATE ( SUM ( ca_invoices[c lineitem_inv_bill] ) ,
     ca_invoices, ca_invoices[lineitems_accountcode] = 121
)

```

----
#### v 1.0.6

```
"""
comment: Changes the relationships on data model to avoid that refund be filtered by cross_hf status filter 
"""

>> +++ measure
CALCULATE(
CALCULATE ( SUM ( ca_invoices[c lineitem_inv_bill] ) ,
     ca_invoices, ca_invoices[lineitems_accountcode] = 310
), CROSSFILTER(CrossJ_HF[cfUNIQUE NUMBER], ID_REG[property_unique_number], OneWay_RightFiltersLeft))

>> --- measure
CALCULATE ( SUM ( ca_invoices[c lineitem_inv_bill] ) ,
     ca_invoices, ca_invoices[lineitems_accountcode] = 310
)

>> +++ measure
occ_refactor = 
CALCULATE(
    DIVIDE([Booked_Nights], [Available_nights]),
    CROSSFILTER(CrossJ_HF[cfUNIQUE NUMBER], ID_REG[property_unique_number],None)
)
```

-----
#### v 1.0.5

```
>> +++ measure
Gross_profit_insurance_% = 
VAR __directCost =  [Agoda_commission] + [Airbnb_commission] + [Booking_commission] +[CTrip_commission]+ [Direct_commission] + [Expedia_commission] + [HomeAway_commission] + [Hostelworld_commission] + [Spotahome_commission] + [Tripadvisor_commission] + [TUI Villas_commission] + [c day transaction fee] + [Disputes]+[Negative Adjustments]
VAR __grossProfit = [Client_Revenue] + __directCost + [Manag. Fee] + [VAT]
VAR __corrected = __grossProfit - [Negative Adjustments] + [negative adjustments report] + [other_revenue] 
VAR __insurance = 0.015 *__corrected
VAR __result = __corrected - __insurance
RETURN
    __result

```

-----
#### 1.0.4
v 1.04

```
>> +++ measure
%_Target_Achieved_refactor = [Gross_Rent] / [Rev_Target_refactor]

>> +++ measure
%_Target_+2 refactor = 
SWITCH(
    TRUE(),
    ISINSCOPE('CALENDAR'[Date].[Day]),
        CALCULATE(
            [%_Target_Achieved_refactor],
            DATEADD('CALENDAR'[Date].[Date], +2,DAY)
        ),
    ISINSCOPE('CALENDAR'[Date].[Month]),
        CALCULATE(
            [%_Target_Achieved_refactor],
            DATEADD('CALENDAR'[Date],+2,MONTH)
        ),
    ISINSCOPE('CALENDAR'[Date].[Quarter]),
        CALCULATE(
            [%_Target_Achieved_refactor],
            DATEADD('CALENDAR'[Date], +2,QUARTER)
        ),
    ISINSCOPE('CALENDAR'[Date].[Year]),
        CALCULATE(
            [%_Target_Achieved_refactor],
            DATEADD('CALENDAR'[Date], +2,YEAR)
        )
)

>> +++ measure
%_Target_+1 _refactor = 
SWITCH(
    TRUE(),
    ISINSCOPE('CALENDAR'[Date].[Day]),
        CALCULATE(
            [%_Target_Achieved_refactor],
            DATEADD('CALENDAR'[Date].[Date], +1,DAY)
        ),
    ISINSCOPE('CALENDAR'[Date].[Month]),
        CALCULATE(
            [%_Target_Achieved_refactor],
            DATEADD('CALENDAR'[Date],+1,MONTH)
        ),
    ISINSCOPE('CALENDAR'[Date].[Quarter]),
        CALCULATE(
            [%_Target_Achieved_refactor],
            DATEADD('CALENDAR'[Date], +1,QUARTER)
        ),
    ISINSCOPE('CALENDAR'[Date].[Year]),
        CALCULATE(
            [%_Target_Achieved_refactor],
            DATEADD('CALENDAR'[Date], +1,YEAR)
        )
)
```

----
#### 1.0.3

```
>> +++ column
cVAT_12,5% = ID_REG[cCOMMISSION_%]*0.125

>> --- column
IF_VAT = IF(CrossJ_HF[Date]>=DATE(2020,7,15) && CrossJ_HF[Date]<=DATE(2021,9,30), CrossJ_HF[cVAT_5%], CrossJ_HF[cVAT20%])

>> +++ column
cVAT12,5% = RELATED(ID_REG[cVAT_12,5%])

>> +++ column
IF_VAT = 
/*IF(
    CrossJ_HF[Date]>=DATE(2020,7,15) && CrossJ_HF[Date]<=DATE(2021,9,30), CrossJ_HF[cVAT_5%], CrossJ_HF[cVAT20%]
    )*/
SWITCH(
    TRUE(),
    CrossJ_HF[Date]>=DATE(2020,7,15) && CrossJ_HF[Date]<=DATE(2021,9,30) && CrossJ_HF[r_neighbourhood] = "Lucia Foster", 0.01,
    CrossJ_HF[Date]>=DATE(2020,7,15) && CrossJ_HF[Date]<=DATE(2021,9,30), CrossJ_HF[cVAT_5%],
    CrossJ_HF[Date]>DATE(2021,9,30) && CrossJ_HF[Date]<=DATE(2022,3,31), CrossJ_HF[cVAT12,5%],
    CrossJ_HF[cVAT20%]
)


>> --- measure
Manag. Fee_% = 
IF(
    MAX(CrossJ_HF[r_neighbourhood]) = "Lucia Foster" && MAX(CrossJ_HF[Date]) >= DATE(2021,9,1), 
    0.2,
    AVERAGEX(ID_REG,
            ID_REG[cCOMMISSION_%]
    )
) 
/*AVERAGEX(ID_REG,
            ID_REG[cCOMMISSION_%]
    )*/

>> +++ measure
 Manag. Fee_% = 
IF(
    MAX(CrossJ_HF[r_neighbourhood]) = "Lucia Foster" && 
        MAX(CrossJ_HF[Date]) >= DATE(2021,8,1) &&
        MAX(CrossJ_HF[Date]) <= DATE(2021,9,30), 
    0.2,
    AVERAGEX(ID_REG,
            ID_REG[cCOMMISSION_%]
    )
) 
/*AVERAGEX(ID_REG,
            ID_REG[cCOMMISSION_%]
    )*/
``` 

------
#### 1.0.2

```
+++  
Negative Adjustments = 
CALCULATE ( SUM ( ca_invoices[c lineitem_inv_bill] ) ,
     ca_invoices, ca_invoices[lineitems_accountcode] = 244
)
---
Negative Adjustments = 
CALCULATE(
CALCULATE ( SUM ( ca_invoices[c lineitem_inv_bill] ) ,
     ca_invoices, ca_invoices[lineitems_accountcode] = 244
),CROSSFILTER())

+++ 
insurance_report = 
VAR __cost =
            CALCULATE (
                SUM ( ca_invoices[c lineitem_inv_bill] ),
                ca_invoices[lineitems_accountcode] = 265
            )
        VAR __availability = [Available_nights]
        VAR __bookedNights =
            CALCULATE ( COUNT ( CrossJ_HF[nights] ), CrossJ_HF[MonthNumber] = MAX('CALENDAR'[MonthNumber]) )
        VAR __insurancePerNight =
            __cost * DIVIDE ( __bookedNights, __availability )
        RETURN
            __insurancePerNight

+++
negative adjustments report =
VAR __cost =
            CALCULATE (
		    	SUM ( ca_invoices[c lineitem_inv_bill] ),
		        --ca_invoices,
		        ca_invoices[lineitems_accountcode] = 244
		    )
		VAR __availability = [Available_nights]
        VAR __bookedNights =
            CALCULATE ( COUNT ( CrossJ_HF[nights] ), CrossJ_HF[MonthNumber] = MAX('CALENDAR'[MonthNumber]) )
        VAR __adjustmentPerNight =
            __cost * DIVIDE ( __bookedNights, __availability )
        RETURN
            __adjustmentPerNight

+++
negative adjustments report =
VAR __cost =
            CALCULATE (
		    	SUM ( ca_invoices[c lineitem_inv_bill] ),
		        --ca_invoices,
		        ca_invoices[lineitems_accountcode] = 244
		    )
		VAR __availability = [Available_nights]
        VAR __bookedNights =
            CALCULATE ( COUNT ( CrossJ_HF[nights] ), CrossJ_HF[MonthNumber] = MAX('CALENDAR'[MonthNumber]) )
        VAR __adjustmentPerNight =
            __cost * DIVIDE ( __bookedNights, __availability )
        RETURN
            __adjustmentPerNight

+++
transaction fee report = SUM(CrossJ_HF[c_day transaction_fee])

+++
other_revenue = 
CALCULATE (
	SUM ( CrossJ_HF[c_day revenue_updated] ),
	NOT( CrossJ_HF[Source_C] ) IN {"Agoda", "Airbnb", "Booking", "CTrip", 
	"Direct", "Expedia","HomeAway", "Hostelworld", "Spotahome", "Tripadvisor", "TUI Villas"}
	)

+++
Gross_profit_insurance_report = 
[Gross_Profit] - [Insurance_of_Landlord_Acc] + [insurance_report] - [Negative Adjustments] + 
                [negative adjustments report] + [other_revenue]
```

