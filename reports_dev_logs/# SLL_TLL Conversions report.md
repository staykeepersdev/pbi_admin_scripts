#  SLL_TLL Conversions report

**What is this file used for:**

This file should contain a summary of relevant changes on this report as well as important information to understand both the model and the visuals constructed. Any reference to measures should be included here as well as any reference necessary to rebuild the report.

**Dev Notes:**
- Deals Measures include all kpi and metrics for business
- viz_metrics includes metrics only used in the context of a specific visual and are based on business metrics

**TODO:**
- [ ] add automatic metadata savings
- [ ] complete notes on each metric
- [ ] refactor M partitions
- [ ] Refactor bullet graphs for increase speed (js look into it)
- [ ] Reduce cardinality (do we need all this data ?)
- [ ] Change fact table to surrogate keys

**Logs**

#### 1.2.1

```
>> +++ query
// Introduced a new dictionary to use as a classifier

let
    // dictionary to use for classification
    #"reasons classes" = #table(type table[#"classes" = text],{{"no availability"},{"covid19"},{"bo unresponsive"},{"guest declined alternative property"},{"student cannot afford the upfront payment"},{"universal credit / dds / housing benefits"},{"unresponsive guest"},{"duplicate"},{"guest with pet"},{"no longer interested"},{"unresponsive"}}),

    // get values from fact tables
    Source = fact_deals,
    #"Removed Other Columns" = Table.SelectColumns(Source,{"lost reason"}),
    #"Filtered Rows" = Table.SelectRows(#"Removed Other Columns", each ([lost reason] <> null)),
    #"Lowercased Text" = Table.TransformColumns(#"Filtered Rows",{{"lost reason", Text.Lower, type text}}),
    #"Trimmed Text" = Table.TransformColumns(#"Lowercased Text",{{"lost reason", Text.Trim, type text}}),
    #"Removed Duplicates" = Table.Distinct(#"Trimmed Text"),
    #"Merged Queries" = Table.FuzzyNestedJoin(#"Removed Duplicates", {"lost reason"}, #"reasons classes", {"classes"}, "reasons classes", JoinKind.LeftOuter, [IgnoreCase=true, IgnoreSpace=true, Threshold=0.8]),
    #"Expanded reasons classes" = Table.ExpandTableColumn(#"Merged Queries", "reasons classes", {"classes"}, {"classes"}),
    #"Renamed Columns" = Table.RenameColumns(#"Expanded reasons classes",{{"classes", "aggr reasons"}}),
    #"Replaced Value" = Table.ReplaceValue(#"Renamed Columns",null,"others",Replacer.ReplaceValue,{"aggr reasons"})
in
    #"Replaced Value"

```

---

#### 1.1.1

```
>> +++ filter
Status = "Won"


>> +++ data
Changed to us regional settings

```


Tag: 1.0.3

Release Date: 29/09/2021

Logs:

27-09-2021 -> Replaced year filter for Line for service filter for the user to have a view by SLL or TLL or Both;

28-09-2021 -> Background for region report

28-09-2021 -> report division by kpi

28-09-2021 -> added small multiples for kpi

28-09-2021 -> added small multiples by source

28-09-2021 -> for conversion rate ytd added a table by source

28-09-2021 -> Added "How to read Bullet Charts" & "How to Drill Visuals" to Help menu
