# Read me

## Purpose

This repository serves as a vault for everything concerning managing and maintaining Powerbi reports/datasets, Bigquery datasets and ETL pipelines. Given its heterogeneous nature it includes not only functioning code but hints and documentation over architecture and design decisions.

It contains the following folders:

**BigQuery:** Includes DML and DLL SQL queries which are relevant or commonly used on reports or scheduled queries. They are just copies and changing then won't impact the production code. Nonetheless, a copy should be kept updated for documentation and reference. 

**Automation:** Includes command lines which automate or bulk perform a certain number of operations on Powerbi, Bigquery or Server. Mainly Powershell or Bash.

**reports_dev_logs:** Logs on Powerbi changes.

**Scripts:** Automation scripts to transform or extract information. Mainly in Python.

**Architecture:** Documentation and guidelines for etl and system architecture. It includes notes on Powerbi report development.

**Dax:** Contains Dax queries useful for debugging or development.

## How to run code inside this repository

Mostly of the code available on this repo is not meant to be ran at the current state, but to serve as a foundation for development and a place for sharing. When needed, each script comments includes instructions on how to run them.

**Some code requires a secrets.json file to run which will have to created by you prior and should include all information required by the script.** The file name should not be changed and it must be included all the time on gitignore.

## Powerbi report deployment:

In order to keep a consistent development and deployment cycle the team as agreed with the following process:

1) During developing stage all reports should be published on **dev** workspace. This also includes changes to current live reports. A copy should be made and a new version tag should be added and then published to dev workspace,

2) Every report needs a version tag which reflects the current state. The agreed convertion is to use **( release, features, fixes )**, therefore, a report with version (1.2.10) on its title, means it is the first release (the firs version) and that since its publishing into production 2 new set of features have been added and 10 round of fixes have been made,

3) The extent of features and fixes on each single release might vary and should be documented on each report log. **Report logs** include changes to report/dataset logic (as a simplification think if M code or Dax code was changed). This means that a simple change on filter value is not to be included on the logs,

4) **For Q&A purposes only a link should be shared with the right stakeholder.** Once both parties agree on the outcome, the report should be moved into one of the production workspaces and all links will be deleted,

5) Users will have access to reports through powerbi workspaces or apps. 

6) Each workspace includes 2 versions of the same report. One is should be the last stable version and a newer one. This allows to quickly replace version on powerbi applications in the case some bug or issue is detected prior go live. Older versions are deleted.

Example:

[![](https://mermaid.ink/img/eyJjb2RlIjoic2VxdWVuY2VEaWFncmFtXG4gICAgYWN0b3IgZHQgYXMgRGF0YSBUZWFtXG4gICAgcGFydGljaXBhbnQgZGV2IGFzIERldmVsb3BtZW50XG4gICAgcGFydGljaXBhbnQgUSZBXG4gICAgcGFydGljaXBhbnQgcHJvZCBhcyBQcm9kdWN0aW9uXG4gICAgYWN0b3IgdXNlciBhcyBTdGFrZWhvbGRlclxuICAgIHJlY3QgcmdiKDE5MSwgMjIzLCAyNTUpXG4gICAgbm90ZSBvdmVyIGR0LHVzZXI6IFBsYW5uaW5nXG4gICAgdXNlciAtLT4-ZHQ6IEkgaGF2ZSBhIG5ldyBSZXF1ZXN0ISAobmV3IGZlYXR1cmUsIHJlcG9ydCwgYnVnKVxuICAgIGR0IC0-PiB1c2VyOiBWYWxpZGF0ZXMgbmVlZHMgYW5kIGFncmVlIG9uIGEgY29tbXVuIHRhc2tcbiAgICBlbmRcbiAgICBub3RlIG92ZXIgZHQsdXNlcjogRXhlY3V0aW9uXG4gICAgZHQtPj5kZXY6IGJ1Z19jb3JyZWN0aW9uLnBiaXggKHYgMS4wLjEpIFxuICAgIGRldi0-PlEmQTogU2VuZCBsaW5rIHRvIHVzZXJcbiAgICB1c2VyLS0-PlEmQTogVmFsaWRhdGVzIGNoYW5nZXNcbiAgICBRJkEtPj5wcm9kOiBidWdfY29ycmVjdGlvbi5wYml4KHYgMS4wLjEpXG4gICAgUSZBLT4-cHJvZDogZG9jdW1lbnQgbG9nIGNoYW5nZXMgb24gcmVwb1xuICAgIHJlY3QgcmdiKDE5MSwgMjIzLCAyNTUpXG4gICAgbm90ZSBvdmVyIGR0LHVzZXI6IFBsYW5uaW5nXG4gICAgdXNlciAtLT4-ZHQ6IEhlbHAuaGVscCB0aGVyZSBpcyBhIG5ldyBidWchXG4gICAgZHQgLT4-IHVzZXI6IFZhbGlkYXRlcyBuZWVkcyBhbmQgYWdyZWUgb24gYSBjb21tdW4gdGFza1xuICAgIGVuZFxuICAgIG5vdGUgb3ZlciBkdCx1c2VyOiBFeGVjdXRpb25cbiAgICBkdC0-PmRldjogYnVnX2NvcnJlY3Rpb24ucGJpeCAodiAxLjAuMikgXG4gICAgZGV2LT4-USZBOiBTZW5kIGxpbmsgdG8gdXNlclxuICAgIHVzZXItLT4-USZBOiBWYWxpZGF0ZXMgY2hhbmdlc1xuICAgIFEmQS0-PnByb2Q6IGJ1Z19jb3JyZWN0aW9uLnBiaXgodiAxLjAuMilcbiAgICBRJkEtPj5wcm9kOiBkb2N1bWVudCBsb2cgY2hhbmdlcyBvbiByZXBvXG4gICAgUSZBLT4-cHJvZDogdmVyc2lvbiAxLjAuMSBzdGF5cyBvbiB3b3Jrc3BhY2UgYnV0IHJlbW92ZWQgZnJvbSBhcHBcbiBcbiAgICAgICAgICAgICIsIm1lcm1haWQiOnsidGhlbWUiOiJkZWZhdWx0In0sInVwZGF0ZUVkaXRvciI6ZmFsc2UsImF1dG9TeW5jIjp0cnVlLCJ1cGRhdGVEaWFncmFtIjpmYWxzZX0)](https://mermaid.live/edit#eyJjb2RlIjoic2VxdWVuY2VEaWFncmFtXG4gICAgYWN0b3IgZHQgYXMgRGF0YSBUZWFtXG4gICAgcGFydGljaXBhbnQgZGV2IGFzIERldmVsb3BtZW50XG4gICAgcGFydGljaXBhbnQgUSZBXG4gICAgcGFydGljaXBhbnQgcHJvZCBhcyBQcm9kdWN0aW9uXG4gICAgYWN0b3IgdXNlciBhcyBTdGFrZWhvbGRlclxuICAgIHJlY3QgcmdiKDE5MSwgMjIzLCAyNTUpXG4gICAgbm90ZSBvdmVyIGR0LHVzZXI6IFBsYW5uaW5nXG4gICAgdXNlciAtLT4-ZHQ6IEkgaGF2ZSBhIG5ldyBSZXF1ZXN0ISAobmV3IGZlYXR1cmUsIHJlcG9ydCwgYnVnKVxuICAgIGR0IC0-PiB1c2VyOiBWYWxpZGF0ZXMgbmVlZHMgYW5kIGFncmVlIG9uIGEgY29tbXVuIHRhc2tcbiAgICBlbmRcbiAgICBub3RlIG92ZXIgZHQsdXNlcjogRXhlY3V0aW9uXG4gICAgZHQtPj5kZXY6IGJ1Z19jb3JyZWN0aW9uLnBiaXggKHYgMS4wLjEpIFxuICAgIGRldi0-PlEmQTogU2VuZCBsaW5rIHRvIHVzZXJcbiAgICB1c2VyLS0-PlEmQTogVmFsaWRhdGVzIGNoYW5nZXNcbiAgICBRJkEtPj5wcm9kOiBidWdfY29ycmVjdGlvbi5wYml4KHYgMS4wLjEpXG4gICAgUSZBLT4-cHJvZDogZG9jdW1lbnQgbG9nIGNoYW5nZXMgb24gcmVwb1xuICAgIHJlY3QgcmdiKDE5MSwgMjIzLCAyNTUpXG4gICAgbm90ZSBvdmVyIGR0LHVzZXI6IFBsYW5uaW5nXG4gICAgdXNlciAtLT4-ZHQ6IEhlbHAuaGVscCB0aGVyZSBpcyBhIG5ldyBidWchXG4gICAgZHQgLT4-IHVzZXI6IFZhbGlkYXRlcyBuZWVkcyBhbmQgYWdyZWUgb24gYSBjb21tdW4gdGFza1xuICAgIGVuZFxuICAgIG5vdGUgb3ZlciBkdCx1c2VyOiBFeGVjdXRpb25cbiAgICBkdC0-PmRldjogYnVnX2NvcnJlY3Rpb24ucGJpeCAodiAxLjAuMikgXG4gICAgZGV2LT4-USZBOiBTZW5kIGxpbmsgdG8gdXNlclxuICAgIHVzZXItLT4-USZBOiBWYWxpZGF0ZXMgY2hhbmdlc1xuICAgIFEmQS0-PnByb2Q6IGJ1Z19jb3JyZWN0aW9uLnBiaXgodiAxLjAuMilcbiAgICBRJkEtPj5wcm9kOiBkb2N1bWVudCBsb2cgY2hhbmdlcyBvbiByZXBvXG4gICAgUSZBLT4-cHJvZDogdmVyc2lvbiAxLjAuMSBzdGF5cyBvbiB3b3Jrc3BhY2UgYnV0IHJlbW92ZWQgZnJvbSBhcHBcbiBcbiAgICAgICAgICAgICIsIm1lcm1haWQiOiJ7XG4gIFwidGhlbWVcIjogXCJkZWZhdWx0XCJcbn0iLCJ1cGRhdGVFZGl0b3IiOmZhbHNlLCJhdXRvU3luYyI6dHJ1ZSwidXBkYXRlRGlhZ3JhbSI6ZmFsc2V9)

```mermaid
sequenceDiagram
    actor dt as Data Team
    participant dev as Development
    participant Q&A
    participant prod as Production
    actor user as Stakeholder
    rect rgb(191, 223, 255)
    note over dt,user: Planning
    user -->>dt: I have a new Request! (new feature, report, bug)
    dt ->> user: Validates needs and agree on a commun task
    end
    note over dt,user: Execution
    dt->>dev: bug_correction.pbix (v 1.0.1) 
    dev->>Q&A: Send link to user
    user-->>Q&A: Validates changes
    Q&A->>prod: bug_correction.pbix(v 1.0.1)
    Q&A->>prod: document log changes on repo
    rect rgb(191, 223, 255)
    note over dt,user: Planning
    user -->>dt: Help.help there is a new bug!
    dt ->> user: Validates needs and agree on a commun task
    end
    note over dt,user: Execution
    dt->>dev: bug_correction.pbix (v 1.0.2) 
    dev->>Q&A: Send link to user
    user-->>Q&A: Validates changes
    Q&A->>prod: bug_correction.pbix(v 1.0.2)
    Q&A->>prod: document log changes on repo
    Q&A->>prod: version 1.0.1 stays on workspace but removed from app
```

### About report logs file

Every Powerbi dataset / report should follow a similar structure for logging changes:

---
**Report/dataset name**

**What is this file used for:** Brief description about the file

**Logs:**

**v 0.0.0 ( version. features. bugs)**

```
comments:
- summary of changes

>>> +++ measure
Available_nights_refactor = 
<New dax or M code>

```

---


**Reports logs should include a list of sources (including tables) being used on the report.**

## Index

### Automation

This scrips contain instructions to run locally, they are not ready for deploy on a ci/cd context.

|Name | Comments | link|
|---------|----------|---------|
 |deploy_to_service.ps1 | Script to change datasets for reports on a given workspace. It searches all reports inside a workspace using  given dataset and migrates it to a new dataset. (No a runnable application requires parameters to be changed in code.) | [link](/Automation/deploy_to_service.ps1)|
 |temporary_deploy.ps1 | Script used to move all reports related with all live into a new workspace for backup. | [link](/Automation/temporary_deploy.ps1)|
 |transfer_schedule_queries_ownership.ps1 | Bigquery instructions to change scheduled query ownership into a service account. (requires bq and gcloud setup on machine prior to run) | [link](Automation/transfer_schedule_queries_ownership.ps1)|


### BigQuery

|Name | Comments | Link|
|---------|----------|---------|
| Bq commands.txt | Useful bq commands for everyday operations like add clustering fields to existing tables |[link](/BigQuery/Bq%20commands.txt)|
| DDL_stk.sql | Copy of SQL scripts to create Views and Schedule queries (in some ocasions it includes DML) | [link](BigQuery/DDL_stk.sql)|



### reports_dev_logs

|Name | Comments | Link|
|---------|----------|---------|
| SLL_TLL Conversions report.md | logs for sll/tll conversion report | [link](/reports_dev_logs/#%20SLL_TLL%20Conversions%20report.md)|
| all_live_log.md | logs for all live report | [link](/reports_dev_logs/all_live_log.md)|


### Scripts


|Name | Comments | Link|
|---------|----------|---------|
| pbi_log_dump.py | Report metadata dumps into json | [link](/Scripts/pbi_log_dump.py)|