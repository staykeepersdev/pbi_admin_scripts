-- DDL

-- stk_reservations
-- based on raw table from etl only fetches more up to date data

CREATE OR REPLACE VIEW `itdata-308413.stk_data.stk_reservations` AS
SELECT 
   *,
    rank() over (partition by api_id order by batch_inserted_at desc) rank
FROM `itdata-308413.landlordhostifydatabase.z_tmp_stk_reservations`
WHERE TRUE
qualify rank = 1;

-- stk_listings
-- based on raw table from etl pipeline only fetches up to date data

CREATE OR REPLACE VIEW `itdata-308413.stk_data.stk_listings` AS
SELECT 
    * ,
    RANK() OVER (partition by api_id order by batch_inserted_at desc) rank
FROM `itdata-308413.landlordhostifydatabase.z_tmp_stklistings`
WHERE TRUE
qualify rank = 1;

-- stk_transactions
-- based on raw table from etl pipeline only fetches up to date data

CREATE OR REPLACE VIEW `itdata-308413.stk_data.stk_transactions` AS
SELECT 
    * ,
    RANK() OVER (partition by api_id order by batch_inserted_at desc) rank
FROM `itdata-308413.landlordhostifydatabase.z_tmp_stk_transactions`
WHERE TRUE
qualify rank = 1;

-- stk_calendar
-- based on raw table from etl pipeline only fetches up to date data

CREATE OR REPLACE VIEW `itdata-308413.stk_data.stk_calendar` AS
SELECT 
    * ,
    RANK() OVER (partition by api_id order by batch_inserted_at desc) rank
FROM `itdata-308413.landlordhostifydatabase.stk_calendardays`
WHERE TRUE
qualify rank = 1;

-- stk_reservations_
CREATE OR REPLACE VIEW `itdata-308413.stk_data.stk_reservations` AS
WITH resPrior82021 AS(
SELECT 
    api_id ,
    guests ,
    nights ,
    source ,
    status ,
    revenue ,
    currency ,
    guest_id ,
    subtotal ,
    base_price ,
    guest_name ,
    listing_id ,
    tax_amount ,
    net_revenue ,
    sum_refunds ,
    CAST (addons_price AS FLOAT64) AS addons_price ,
    advance_days ,
    cleaning_fee ,
    extras_price ,
    payout_price ,
    cancel_policy ,
    listing_title ,
    owner_revenue ,
    integration_id ,
    security_price ,
    service_charge ,
    extras_price_ex ,
    price_per_night ,
    transaction_fee ,
    cancellation_fee ,
    extras_price_inc ,
    integration_name ,
    integration_type ,
    listing_nickname ,
    confirmation_code ,
    parent_listing_id ,
    channel_commission ,
    listing_channel_id ,
    integration_nickname ,
    channel_reservation_id ,
    payout_by_channel ,
    paid_sum ,
    paid_part ,
    owner_commission ,
    extra_guest_price ,
    CAST(updated_at AS TIMESTAMP ) updated_at ,		
    CAST(created_at	AS TIMESTAMP ) created_at ,
    CAST(cancelled_at AS TIMESTAMP ) cancelled_at ,		
    CAST(checkIn AS TIMESTAMP ) checkIn ,	
    CAST(checkOut AS TIMESTAMP ) chckOut ,	
    CAST(confirmed_at AS TIMESTAMP ) confirmed_at ,		
    planned_arrival ,	
    custom_field_478 ,	
    custom_field_485 ,	
    CAST(custom_field_487 AS NUMERIC) custom_field_487 ,	
    CAST(custom_field_488 AS NUMERIC) custom_field_488 ,		
    CAST(custom_field_489 AS NUMERIC) custom_field_489 ,
    CAST(custom_field_490 AS NUMERIC) custom_field_490 ,	
    CAST(custom_field_493 AS NUMERIC) custom_field_493 ,	
    CAST(custom_field_494 AS NUMERIC) custom_field_494 ,	
    CAST(custom_field_495 AS NUMERIC) custom_field_495 ,	
    CAST(custom_field_496 AS NUMERIC) custom_field_496 ,	
    CAST(custom_field_498 AS NUMERIC) custom_field_498 ,	
    CAST(custom_field_536 AS NUMERIC) custom_field_536 ,		
    custom_field_479 ,	
    custom_field_673 ,	
    custom_field_620 ,	
    custom_field_491 ,	
    custom_field_544 ,
    custom_field_671 ,	
    custom_field_672 ,	
    custom_field_564 ,
    custom_field_579 	
FROM `itdata-308413.landlordhostifydatabase.api_data_apireservation_latest`
WHERE checkOut <= '2021-08-31'),
resAfter082021 AS (
    SELECT 
    *,
        rank() over (partition by api_id order by batch_inserted_at desc) rank
    FROM `itdata-308413.landlordhostifydatabase.z_tmp_stk_reservations`
    WHERE TRUE
    AND checkOut > '2021-08-31'
    QUALIFY rank = 1
)
SELECT * 
FROM resPrior82021
UNION ALL 
SELECT 
    api_id ,
    guests ,
    nights ,
    source ,
    status ,
    revenue ,
    currency ,
    guest_id ,
    subtotal ,
    base_price ,
    guest_name ,
    listing_id ,
    tax_amount ,
    net_revenue ,
    sum_refunds ,
    CAST (addons_price AS FLOAT64) AS addons_price ,
    advance_days ,
    cleaning_fee ,
    extras_price ,
    payout_price ,
    cancel_policy ,
    listing_title ,
    owner_revenue ,
    integration_id ,
    security_price ,
    service_charge ,
    extras_price_ex ,
    price_per_night ,
    transaction_fee ,
    cancellation_fee ,
    extras_price_inc ,
    integration_name ,
    integration_type ,
    listing_nickname ,
    confirmation_code ,
    parent_listing_id ,
    channel_commission ,
    listing_channel_id ,
    integration_nickname ,
    channel_reservation_id ,
    payout_by_channel ,
    paid_sum ,
    paid_part ,
    owner_commission ,
    extra_guest_price ,
    CAST(updated_at AS TIMESTAMP ) updated_at ,		
    CAST(created_at	AS TIMESTAMP ) created_at ,
    cancelled_at ,		
    CAST(checkIn AS TIMESTAMP ) checkIn ,	
    CAST(checkOut AS TIMESTAMP ) chckOut ,	
    confirmed_at ,		
    CAST(planned_arrival AS STRING ) planned_arrival ,	
    custom_field_478 ,	
    custom_field_485 ,	
    custom_field_487 ,	
    custom_field_488 ,		
    custom_field_489 ,
    custom_field_490 ,	
    custom_field_493 ,	
    custom_field_494 ,	
    custom_field_495 ,	
    custom_field_496 ,	
    custom_field_498 ,	
    custom_field_536 ,		
    custom_field_479 ,	
    custom_field_673 ,	
    custom_field_620 ,	
    custom_field_491 ,	
    custom_field_544 ,
    custom_field_671 ,	
    custom_field_672 ,	
    CAST(custom_field_564 AS STRING) custom_field_564,
    CAST(custom_field_579 AS STRING) custom_field_579
FROM resAfter082021  ;

-- sll_tll_reservations
CREATE OR REPLACE VIEW `itdata-308413.tll_sll_data.tll_sll_reservations` AS
SELECT 
    * ,
    RANK() OVER (partition by api_id order by batch_inserted_at desc) rank
FROM `itdata-308413.landlordhostifydatabase.z_tmp_tll_sll_reservations`
WHERE TRUE
qualify rank = 1;


-- stk_listings
-- create table for listing accupancy per month with memmory
CREATE OR REPLACE TABLE `itdata-308413.stk_data.stk_listing_movements` 
PARTITION BY DATE(accounting_date) 
CLUSTER BY property_unique_number, neighborhood, year, month
AS
WITH tmp AS(
    SELECT 
        listing_id, 
        batch_inserted_at AS accounting_date,
        date,
        CASE 
            WHEN ( status = "available" AND price = 1000 ) OR status = "unavailable" THEN 1 ELSE 0
        END AS unavailable_flag,
        CASE 
            WHEN ( status = "available" OR status = "booked" ) AND NOT ( status = "available" AND price = 1000 ) THEN 1 ELSE 0
        END AS available_flag
    FROM `itdata-308413.landlordhostifydatabase.stk_calendardays`
), 
tmp2 AS(
    SELECT 
        *,
        LAG(accounting_date) OVER (PARTITION BY listing_id, date ORDER BY accounting_date) AS back_entry,
        CASE 
            WHEN LAG(available_flag) OVER (PARTITION BY listing_id, date ORDER BY accounting_date) IS NULL 
            THEN available_flag
            ELSE available_flag - LAG(available_flag) OVER (PARTITION BY listing_id, date ORDER BY accounting_date) 
        END AS var_available,
        CASE 
            WHEN LAG(unavailable_flag) OVER (PARTITION BY listing_id, date ORDER BY accounting_date) IS NULL 
            THEN unavailable_flag
            ELSE unavailable_flag - LAG(unavailable_flag) OVER (PARTITION BY listing_id, date ORDER BY accounting_date) 
        END AS var_unavailable
    FROM tmp
    ORDER BY accounting_date
),
tmp3 AS(
    SELECT 
        listing_id,
        accounting_date ,
        EXTRACT( YEAR FROM date) AS year,
        EXTRACT(month FROM date) AS month,
        SUM(var_available) AS var_available,
        SUM(var_unavailable) AS var_unavailable
    FROM tmp2
    GROUP BY 
        listing_id,
        year,
        month,
        accounting_date
    ORDER BY year ,month
)
SELECT *
FROM tmp3
LEFT JOIN (SELECT DISTINCT api_id AS id, custom_field_478 AS property_id 
                FROM `itdata-308413.landlordhostifydatabase.z_tmp_stklistings`) AS property
    ON tmp3.listing_id = property.id
LEFT JOIN (SELECT property_unique_number, neighborhood FROM `itdata-308413.landlordhostifydatabase.id_register`) AS id_reg
    ON property. property_id = id_reg.property_unique_number
WHERE year  > ( 
        SELECT EXTRACT(YEAR FROM MAX(created_at))-1 
        FROM `itdata-308413.stk_data.stk_reservations`);





-- stk_neighborhoods_occupancy
--- Materialized View for occupancy aggregation over neighborhoods
SELECT 
    neighborhood, 
    year,
    month,
    accounting_date,
    SUM(var_available) OVER(PARTITION BY year, month ORDER BY accounting_date ),
    SUM(var_unavailable) OVER(PARTITION BY year, month ORDER BY accounting_date ) 
FROM `itdata-308413.stk_data.stk_listing_movements`
WHERE property_unique_number IS NOT NULL AND property_unique_number > 0;

-- stk_ovhCosts_byProperty
--- View with costs per property including memory
WITH tmp_property AS (
	SELECT 
	    lineitems_tracking_option,
	    REGEXP_EXTRACT(lineitems_tracking_option, r'^(.+?),') AS property_unique_number,
	    UNIX_SECONDS(_sdc_batched_at) AS accounting_date_index,
	    EXTRACT (DATE FROM _sdc_batched_at) AS accounting_date,
	    _sdc_batched_at,
	    lineitems_accountcode,
	    total,
	    EXTRACT(YEAR from date) AS year,
	    EXTRACT(MONTH from date) AS month,
	    invoiceid,
	    date
	FROM `xero-245302.xero_cl_acc.XERO_UNNEST_invoices` 
	WHERE REGEXP_EXTRACT(lineitems_tracking_option, r'^(.+?),') IS NOT NULL
),
tmp_lag AS(
	SELECT
		property_unique_number, 
		year, 
		month, 
		lineitems_accountcode AS account_code,
		_sdc_batched_at AS accounting_datetime,
		CASE 
		   WHEN LAG(total) OVER (PARTITION BY invoiceid, date ORDER BY _sdc_batched_at) IS NULL 
		   THEN total
		   ELSE total - LAG(total) OVER (PARTITION BY invoiceid, date ORDER BY _sdc_batched_at) 
		END AS delta,
		LAG(total) OVER (PARTITION BY invoiceid ORDER BY _sdc_batched_at) AS earlier,
		invoiceid,
		total,
	FROM tmp_property
)
SELECT
	property_unique_number,
	year, 
	month, 
	account_code,
	accounting_datetime,
	SUM(delta) OVER(PARTITION BY property_unique_number, year, month, account_code ORDER BY accounting_datetime ) AS costs
FROM tmp_lag


-- stk_reservations_direct_pl
--- Reservation based information 
CREATE OR REPLACE TABLE `itdata-308413.stk_data.stk_reservations_direct_pl` 
PARTITION BY accounting_date
CLUSTER BY checkOut, property_id
AS
  WITH res AS (
    SELECT 
        api_id ,
        nights ,
        source ,
        status ,
        revenue ,
        currency ,
        base_price ,
        listing_id ,
        tax_amount ,
        sum_refunds AS refunds ,
        CAST (addons_price AS FLOAT64) AS addons_price ,
        advance_days ,
        cleaning_fee ,
        extras_price ,
        payout_price ,
        owner_revenue ,
        security_price ,
        service_charge ,
        extras_price_ex ,
        price_per_night ,
        transaction_fee ,
        cancellation_fee ,
        extras_price_inc ,
        listing_nickname ,
        confirmation_code ,
        listing_channel_id	,
        channel_commission ,
        channel_reservation_id ,
        paid_sum ,
        paid_part ,
        owner_commission ,
        extra_guest_price ,	
        CAST(created_at	AS TIMESTAMP ) created_at ,
        CAST(checkIn AS TIMESTAMP ) checkIn ,	
        CAST(checkOut AS TIMESTAMP ) checkOut ,		
        custom_field_478 ,
        '2020-08-01' AS batch_inserted_at
    FROM `itdata-308413.landlordhostifydatabase.api_data_apireservation_latest`
    WHERE checkOut <= '2021-08-31'
    UNION ALL 
    SELECT 
        api_id AS `res id`,
        nights ,
        source ,
        status ,
        revenue ,
        currency ,
        base_price ,
        listing_id ,
        tax_amount ,
        sum_refunds AS refunds ,
        CAST (addons_price AS FLOAT64) AS addons_price ,
        advance_days ,
        cleaning_fee ,
        extras_price ,
        payout_price ,
        owner_revenue ,
        security_price ,
        service_charge ,
        extras_price_ex ,
        price_per_night ,
        transaction_fee ,
        cancellation_fee ,
        extras_price_inc ,
        listing_nickname ,
        confirmation_code ,
        listing_channel_id	,
        channel_commission ,
        channel_reservation_id ,
        paid_sum ,
        paid_part ,
        owner_commission ,
        extra_guest_price ,
        CAST(created_at	AS TIMESTAMP ) created_at ,	
        CAST(checkIn AS TIMESTAMP ) checkIn ,	
        CAST(checkOut AS TIMESTAMP ) checkOut ,		
        custom_field_478 ,
        batch_inserted_at
    FROM `itdata-308413.landlordhostifydatabase.z_tmp_stk_reservations`
    WHERE checkOut > '2021-08-31'
    )
    SELECT 
        * except (custom_field_478,  created_at, checkIn , checkOut, property_unique_number, commission),
        EXTRACT (DATE FROM created_at) AS created_at,
        EXTRACT (DATE FROM checkIn) AS checkIn,
        EXTRACT (DATE FROM checkOut) AS checkOut,
        EXTRACT (DATE FROM batch_inserted_at) AS accounting_date,
        UNIX_SECONDS(batch_inserted_at) AS accounting_date_index,
        CAST(custom_field_478 AS INT64) AS property_id,
        CASE neighborhood
            WHEN "Our Lady of Fatima Residence" THEN 0.1
            WHEN "Liv Student" THEN 0.2
            WHEN "Dubailand" THEN 0.1
        ELSE ROUND(id_reg.commission / 1.2, 3) END AS commission,
        channel_commission / nights AS channel_commission_perNight,
        cleaning_fee / nights AS cleaning_fee_perNight,
        service_charge / nights AS service_charge_perNight,
        transaction_fee / nights AS transaction_fee_perNight,
        RANK() OVER (partition by api_id order by batch_inserted_at desc) rank
    FROM res
    LEFT JOIN (SELECT property_unique_number, neighborhood, commission FROM`itdata-308413.landlordhostifydatabase.id_register`) AS id_reg
    ON custom_field_478 = property_unique_number
    WHERE EXTRACT (YEAR FROM checkOut) > EXTRACT (YEAR FROM CURRENT_TIMESTAMP()) - 2;


----
CREATE OR REPLACE TABLE `itdata-308413.stk_data.stk_reservations_direct_pl` 
PARTITION BY accounting_date
CLUSTER BY checkOut, property_id
AS
WITH res AS (
    SELECT 
        api_id,
        nights ,
        source ,
        status ,
        revenue ,
        currency ,
        base_price ,
        listing_id ,
        tax_amount ,
        sum_refunds AS refunds ,
        CAST (addons_price AS FLOAT64) AS addons_price ,
        advance_days ,
        cleaning_fee ,
        extras_price ,
        payout_price ,
        owner_revenue ,
        security_price ,
        service_charge ,
        extras_price_ex ,
        price_per_night ,
        transaction_fee + IFNULL(custom_field_487,0) AS transaction_fee,
        cancellation_fee ,
        extras_price_inc ,
        listing_nickname ,
        confirmation_code ,
        listing_channel_id	,
        channel_commission ,
        channel_reservation_id ,
        paid_sum ,
        paid_part ,
        owner_commission ,
        extra_guest_price ,
        CAST(created_at	AS TIMESTAMP ) created_at ,	
        CAST(checkIn AS TIMESTAMP ) checkIn ,	
        CAST(checkOut AS TIMESTAMP ) checkOut ,		
        custom_field_478 ,
        batch_inserted_at
    FROM `itdata-308413.landlordhostifydatabase.z_tmp_stk_reservations`
    )
SELECT 
    * except (custom_field_478,  created_at, checkIn , checkOut, property_unique_number, commission),
    EXTRACT (DATE FROM created_at) AS created_at,
    EXTRACT (DATE FROM checkIn) AS checkIn,
    EXTRACT (DATE FROM checkOut) AS checkOut,
    EXTRACT (DATE FROM batch_inserted_at) AS accounting_date,
    UNIX_SECONDS(batch_inserted_at) AS accounting_date_index,
    CAST(custom_field_478 AS INT64) AS property_id,
    CASE neighborhood
        WHEN "Our Lady of Fatima Residence" THEN 0.1
        WHEN "Liv Student" THEN 0.2
        WHEN "Dubailand" THEN 0.1
        WHEN "Medway" THEN 0.1
        WHEN "Redvers Tower" THEN 0.18
    ELSE ROUND(id_reg.commission / 1.2, 3) END AS commission,
    channel_commission / nights AS channel_commission_perNight,
    cleaning_fee / nights AS cleaning_fee_perNight,
    service_charge / nights AS service_charge_perNight,
    transaction_fee / nights AS transaction_fee_perNight,
    RANK() OVER (partition by api_id order by batch_inserted_at desc) rank
FROM res
LEFT JOIN (SELECT property_unique_number, neighborhood, commission FROM`itdata-308413.landlordhostifydatabase.id_register`) AS id_reg
ON custom_field_478 = property_unique_number
WHERE EXTRACT (YEAR FROM checkOut) > EXTRACT (YEAR FROM CURRENT_TIMESTAMP()) - 2;

