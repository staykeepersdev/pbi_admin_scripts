CREATE OR REPLACE TABLE `itdata-308413.stk_data.stk_ovhCosts_byProperty` 
PARTITION BY DATE(accounting_datetime)
CLUSTER BY accounting_datetime, account_code, property_unique_number AS
WITH tmp_property AS (
	SELECT 
	    lineitems_tracking_option,
	    REGEXP_EXTRACT(lineitems_tracking_option, r'^(.+?),') AS property_unique_number,
	    UNIX_SECONDS(_sdc_batched_at) AS accounting_date_index,
	    EXTRACT (DATE FROM _sdc_batched_at) AS accounting_date,
	    _sdc_batched_at,
	    lineitems_accountcode,
	    total,
	    EXTRACT(YEAR from date) AS year,
	    EXTRACT(MONTH from date) AS month,
	    invoiceid,
	    date
	FROM `xero-245302.xero_cl_acc.XERO_UNNEST_invoices` 
	WHERE REGEXP_EXTRACT(lineitems_tracking_option, r'^(.+?),') IS NOT NULL
),
tmp_lag AS(
	SELECT
		property_unique_number, 
		year, 
		month, 
		lineitems_accountcode AS account_code,
		_sdc_batched_at AS accounting_datetime,
		CASE 
		   WHEN LAG(total) OVER (PARTITION BY invoiceid, lineitems_accountcode, date ORDER BY _sdc_batched_at) IS NULL 
		   THEN total
		   ELSE total - LAG(total) OVER (PARTITION BY invoiceid, lineitems_accountcode, date ORDER BY _sdc_batched_at) 
		END AS delta,
		LAG(total) OVER (PARTITION BY invoiceid ORDER BY _sdc_batched_at) AS earlier,
		invoiceid,
		total,
	FROM tmp_property
)
SELECT
	property_unique_number,
	year, 
	month, 
	account_code,
	accounting_datetime,
    total,
	SUM(delta) OVER(PARTITION BY property_unique_number, year, month, account_code ORDER BY accounting_datetime ) AS costs
FROM tmp_lag    

--WHERE property_unique_number 
--    IN ('5964','5958','5961','5968','5959','5956','5957','5967','5966','7349','6968','7350',
--    '7837','7838','7839','7840','5962','5965','5963','7351','5960')
 --   AND account_code LIKE '265'