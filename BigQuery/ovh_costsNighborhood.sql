CREATE OR REPLACE TABLE `itdata-308413.stk_data.ovhCosts_landlordReport` 
OPTIONS(
    description = "Overhead cost by neighborhood and report date for actual and memory options. Memory values already considering cutoff date"
)
AS
WITH tmp AS (
SELECT
	neighborhood,
	tmp_lag.accounting_datetime,
	tmp_lag.account_code,
	tmp_lag.costs,
    tmp_lag.year,
    tmp_lag.month,
	DATE(tmp_lag.year, tmp_lag.month, 1 ) AS reporting_date,
	rpd.cutOff,
	CASE WHEN EXTRACT(DATE FROM accounting_datetime) <= rpd.cutOff THEN 'memory' ELSE 'actual' END AS report_type
FROM `itdata-308413.stk_data.stk_ovhCosts_byProperty`AS tmp_lag
LEFT JOIN `itdata-308413.landlordhostifydatabase.id_register` id
ON tmp_lag.property_unique_number = CAST(id.property_unique_number AS STRING)
LEFT JOIN (
		SELECT 
			neighbourhood,
			reporting_date,
			cutOff
		FROM `itdata-308413.stk_data.reporting_cutoff_map`
	) rpd
ON  DATE(tmp_lag.year, tmp_lag.month, 1 ) = reporting_date 
	AND LOWER(neighborhood) = rpd.neighbourhood
),

max_table AS(
    SELECT 
        reporting_date, 
        neighborhood, 
        account_code,
        report_type,
        accounting_datetime,
        costs,
        
        MAX(accounting_datetime) OVER(PARTITION BY year, month, account_code, neighborhood,report_type) AS max_date
    FROM tmp
)

SELECT 
    reporting_date, 
    neighborhood , 
    account_code, 
    report_type,
    --costs
    SUM(costs) as costs
FROM (SELECT DISTINCT * FROM max_table)
WHERE max_date = accounting_datetime 
--AND neighborhood LIKE 'Exeter' AND account_code LIKE '265'
GROUP BY reporting_date, neighborhood , account_code, report_type