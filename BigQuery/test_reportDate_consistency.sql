/********

// Name: test_reportDate_consistency.sql
// Description: Tests if the max used date for a given reservation which spans over 2 subsquent months is actually time consistent. For example, max date for November is 11/11/year but for December is 01/10/year

Query compares for each reservation the max date chosen for the previows month and calculates a difference. Below or equal to zero results are ok. Exception if diff returns positive.
// Tables: 
    itdata-308413.stk_data.stk_reservations_direct_report;
// Variables:

*********/

WITH tmp AS(
    SELECT 
        api_id,
        batch_inserted_at,
        LAG(batch_inserted_at) OVER(PARTITION BY api_id ORDER BY reportMonthBeginDate ) AS earlier,
        reportMonthBeginDate,
        IFNULL(
            DATETIME_DIFF(
                LAG(batch_inserted_at) OVER(PARTITION BY api_id ORDER BY reportMonthBeginDate ),
                batch_inserted_at, 
                HOUR )
            ,0) AS calc_diff
    FROM `itdata-308413.stk_data.stk_reservations_direct_report` 
    WHERE label LIKE 'memory'
        AND status IN ('accepted','extended')
    ORDER BY api_id
)
SELECT 
    *,
    CASE WHEN calc_diff <= 0 THEN 0 ELSE 1 END AS test
FROM tmp 