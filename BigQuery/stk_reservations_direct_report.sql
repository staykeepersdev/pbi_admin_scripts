/********

// Name: stk_reservations_direct_report.sql
// Description: 
From the list of reservations including all direct cost and revenue selects the ones which are the last entries ("actual") and the ones which are the latest before a given report date ("memory")

The output is a table containing both actual and memory reservations

// Tables: 
    itdata-308413.stk_data.reporting_cutoff_map`
    itdata-308413.stk_data.stk_reservations_direct_pl
// Variables:

*********/

WITH inc_table AS (
    SELECT 
        *,
        CASE WHEN DATE(batch_inserted_at) < DATE(cutOff) THEN 1 ELSE 0 END AS included_report_flag
    FROM `itdata-308413.stk_data.stk_reservations_direct_pl`,
    UNNEST(GENERATE_DATE_ARRAY(DATE_TRUNC(checkIn,MONTH), checkOut, INTERVAL 1 MONTH)) AS reportMonthBeginDate 
    LEFT JOIN (
        SELECT 
            neighbourhood,
            reporting_date,
            cutOff
        FROM `itdata-308413.stk_data.reporting_cutoff_map`
    ) AS date_report
    ON date_report.reporting_date = reportMonthBeginDate AND
       date_report.neighbourhood =  LOWER(neighborhood)
),

memory_report AS(
    SELECT 
        * EXCEPT (included_report_flag, reporting_date, neighbourhood) ,
        MAX(batch_inserted_at) OVER (PARTITION BY api_id, reporting_date) date_max,
        'memory' AS label    
    FROM inc_table 
    WHERE included_report_flag = 1
),

memory_corrected AS(
    SELECT 
        *,
        CASE 
            WHEN LAG(date_max) OVER(PARTITION BY api_id ORDER BY reportMonthBeginDate) > date_max THEN LAG(date_max) OVER(PARTITION BY api_id ORDER BY reportMonthBeginDate) ELSE date_max END AS date_max_corrected
    FROM memory_report
),

last_report AS(
    SELECT 
        *,
        DATE(NULL) AS cutOff,
        'actual' AS label
    FROM `itdata-308413.stk_data.stk_reservations_direct_pl`,
    UNNEST(GENERATE_DATE_ARRAY(DATE_TRUNC(checkIn,MONTH), checkOut, INTERVAL 1 MONTH)) AS reportMonthBeginDate
    WHERE rank = 1
)

SELECT DISTINCT  * EXCEPT (date_max, date_max_corrected) FROM (SELECT * FROM memory_corrected WHERE batch_inserted_at = date_max_corrected)
--WHERE api_id = 841511
UNION ALL 
SELECT * FROM last_report
--WHERE api_id = 841511
