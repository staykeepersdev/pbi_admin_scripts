 WITH ovh_class AS(
     SELECT 
                reporting_date,
                report_type,
                LOWER(neighborhood) AS neighborhood,
                account_code,
                costs,
                CASE 
                    WHEN account_code LIKE '265' THEN 'insurance'
                    WHEN account_code LIKE '309' THEN 'disputes'
                    WHEN account_code LIKE '244' THEN 'negative_adjustments'
                    WHEN account_code LIKE '126' THEN 'positive_adjustments'
                    WHEN account_code LIKE '310' THEN 'refund'
                    WHEN account_code LIKE '122' THEN 'other_income'
                    WHEN account_code LIKE '121' THEN 'cleaning_fee_refund'
                    ELSE 'no_category'
                END AS category
            FROM `itdata-308413.stk_data.ovhCosts_landlordReport`
 ),
 
 ovh AS(
    SELECT *  
    FROM(
            SELECT 
                reporting_date,
                report_type,
                neighborhood,
                category,
                SUM(costs) AS costs,
            FROM ovh_class 
            GROUP BY reporting_date, report_type,neighborhood, category

        )
        PIVOT( SUM(costs) FOR category IN ('insurance', 'disputes','negative_adjustments', 'positive_adjustments', 'refund', 
        'other_income','cleaning_fee_refund', 'no_category') )
 ),

table_window AS 
(
    SELECT 
        api_id,
        nights,
        LOWER(source) AS source,
        price_per_night,
        paid_part,
        LOWER(neighborhood) AS neighborhood ,
        checkIn,
        checkOut,
        CASE 
            WHEN reportMonthBeginDate > checkIn THEN reportMonthBeginDate 
            WHEN reportMonthBeginDate <= checkIn THEN checkIn
            ELSE checkIn
        END AS windowStart,
        CASE 
            WHEN LAST_DAY(reportMonthBeginDate, MONTH) < checkOut THEN LAST_DAY(reportMonthBeginDate, MONTH) + 1
            WHEN LAST_DAY(reportMonthBeginDate, MONTH) >= checkOut THEN checkOut
            ELSE checkOut
        END AS windowEnd,
        reportMonthBeginDate AS reportDate,
        channel_commission_perNight,
        cleaning_fee_perNight,
        service_charge_perNight,
        transaction_fee_perNight,
        IF(neighborhood LIKE 'Lucia Foster' 
            and (reportMonthBeginDate = '2021-08-01' 
                or reportMonthBeginDate = '2021-09-01'),0.2, commission ) AS commission_percentage,
        label,
        vat_table.vat_tax AS vat_tax
    FROM `itdata-308413.stk_data.stk_reservations_direct_report`
    LEFT JOIN(
        SELECT DISTINCT begin_month, vat_tax FROM `itdata-308413.DEV.dev_reporting_date`
    ) as vat_table
    ON reportMonthBeginDate = vat_table.begin_month
    WHERE status IN ('accepted', 'extended')
),

table_reportDays AS (
    SELECT 
        *,
        DATE_DIFF(windowEnd, windowStart, DAY) AS report_days
    FROM table_window 
),

table_costRevenue AS(
    SELECT 
        *,
        
        -- Revenue
        IFNULL( IF(source LIKE 'airbnb',report_days * price_per_night, 0), 0) AS revenue_airbnb,
        IFNULL( IF(source LIKE 'booking.com',report_days * price_per_night, 0), 0) AS revenue_booking,
        IFNULL( IF(source LIKE 'direct',report_days * price_per_night, 0), 0) AS revenue_direct,
        IFNULL( IF(source NOT IN ('airbnb', 'booking.com', 'direct'),report_days * price_per_night, 0), 0) AS revenue_other,
        
        -- Costs
        IFNULL(report_days * cleaning_fee_perNight * -1, 0) AS cleaning_fee,
        IFNULL(report_days * service_charge_perNight * -1, 0) AS service_charge,
        IFNULL(report_days * transaction_fee_perNight * -1, 0) AS transaction_fee,

        -- Commissions
        IFNULL( IF(source LIKE 'airbnb',report_days * channel_commission_perNight * -1, 0), 0) AS commission_airbnb,
        IFNULL( IF(source LIKE 'booking.com',report_days * channel_commission_perNight * -1, 0), 0) AS commission_booking,
        IFNULL( IF(source LIKE 'direct',report_days * channel_commission_perNight * -1, 0), 0) AS commission_direct,
        IFNULL( IF(source NOT IN ('airbnb', 'booking.com', 'direct'),report_days * channel_commission_perNight * -1, 0), 0) AS commission_other,

    FROM table_reportDays
), 

/*
    Groups by payment situations either all payments or only full payment
*/
table_groupBy_everyPayment AS (
    SELECT 
        neighborhood,  
        reportDate, 
        label,
        AVG(IFNULL(commission_percentage, 0)) AS management_fee_perc ,
        AVG(vat_tax) AS vat_tax ,
        SUM(report_days) AS booked_nights , 
        SUM(revenue_airbnb) AS revenue_airbnb ,
        SUM(revenue_booking) AS revenue_booking ,
        SUM(revenue_direct) AS revenue_direct ,
        SUM(revenue_other) AS revenue_other ,
        SUM(cleaning_fee) AS cleaning_fee ,
        SUM(service_charge) AS service_charge ,
        SUM(transaction_fee ) AS transaction_fee,
        SUM(commission_airbnb) AS commission_airbnb ,
        SUM(commission_booking) AS commission_booking ,
        SUM(commission_direct) AS commission_direct ,
        SUM(commission_other) AS commission_other,
        'all payments' AS payment_filter
    FROM table_costRevenue
    GROUP BY neighborhood,  reportDate, label
),

table_groupBy_fullPayment AS (
    SELECT 
        neighborhood,  
        reportDate, 
        label,
        AVG(IFNULL(commission_percentage, 0)) AS management_fee_perc ,
        AVG(vat_tax) AS vat_tax ,
        SUM(report_days) AS booked_nights , 
        SUM(revenue_airbnb) AS revenue_airbnb ,
        SUM(revenue_booking) AS revenue_booking ,
        SUM(revenue_direct) AS revenue_direct ,
        SUM(revenue_other) AS revenue_other ,
        SUM(cleaning_fee) AS cleaning_fee ,
        SUM(service_charge) AS service_charge ,
        SUM(transaction_fee ) AS transaction_fee,
        SUM(commission_airbnb) AS commission_airbnb ,
        SUM(commission_booking) AS commission_booking ,
        SUM(commission_direct) AS commission_direct ,
        SUM(commission_other) AS commission_other,
        'fully paid' AS payment_filter
    FROM table_costRevenue
    WHERE paid_part LIKE 'full' OR source LIKE 'airbnb'
    GROUP BY neighborhood,  reportDate, label
),

table_union AS(

    SELECT tgep.*, ovh.* EXCEPT(reporting_date, neighborhood,report_type)  FROM table_groupBy_everyPayment AS tgep
    LEFT JOIN ovh 
    ON tgep.reportDate = ovh.reporting_date AND tgep.neighborhood = ovh.neighborhood AND tgep.label = ovh.report_type

    UNION ALL

    SELECT tgep.*, ovh.* EXCEPT(reporting_date, neighborhood,report_type) FROM table_groupBy_fullPayment AS tgep
    LEFT JOIN ovh 
    ON tgep.reportDate = ovh.reporting_date AND tgep.neighborhood = ovh.neighborhood AND tgep.label = ovh.report_type

),

table_gr AS (
    SELECT 
        * ,
        (revenue_airbnb + revenue_booking + revenue_direct + revenue_other + 
        commission_airbnb + commission_booking + commission_direct + commission_other + transaction_fee + IFNULL(positive_adjustments, 0) - IFNULL(disputes ,0) - IFNULL(insurance ,0) - IFNULL(negative_adjustments, 0) ) AS gross_rent 
    FROM table_union 
),

table_result AS (
    SELECT 
        *,
        IFNULL(gross_rent * management_fee_perc * -1, 0) AS management_fee,
        IFNULL(gross_rent * management_fee_perc * -1, 0) * vat_tax  AS vat,
        gross_rent + IFNULL(gross_rent * management_fee_perc , 0) * vat_tax * -1 + IFNULL((gross_rent * management_fee_perc * -1),0) AS gross_profit
    FROM table_gr
)
SELECT * FROM(
    SELECT neighborhood, reportDate, label, payment_filter, management_fee, 
    management_fee_perc, vat, vat_tax, CAST(booked_nights AS FLOAT64) AS booked_nights, revenue_airbnb, revenue_booking,revenue_direct, revenue_other, cleaning_fee,
    service_charge, transaction_fee, commission_airbnb, commission_booking,
    commission_direct, commission_other, CAST(IFNULL(-insurance, 0) AS FLOAT64) AS insurance,
    CAST(IFNULL(-disputes, 0) AS FLOAT64) AS disputes, CAST(IFNULL(-negative_adjustments, 0) AS FLOAT64) AS negative_adjustments,
    CAST(IFNULL(positive_adjustments, 0) AS FLOAT64) AS positive_adjustments, CAST(IFNULL(-refund, 0) AS FLOAT64) AS refund,
    CAST(IFNULL(other_income, 0) AS FLOAT64) AS other_income, CAST(IFNULL(cleaning_fee_refund,0) AS FLOAT64) AS cleaning_fee_refund,
    CAST(IFNULL(no_category, 0) AS FLOAT64) AS no_category, gross_rent, gross_profit
    FROM table_result
)
UNPIVOT( values FOR attributes  IN (management_fee,management_fee_perc, vat, vat_tax, booked_nights , revenue_airbnb, revenue_booking, revenue_direct, revenue_other, cleaning_fee, service_charge, transaction_fee, commission_airbnb, commission_booking,  commission_direct, commission_other, insurance, disputes, negative_adjustments, positive_adjustments, refund, other_income, cleaning_fee_refund,no_category, gross_rent, gross_profit) )

