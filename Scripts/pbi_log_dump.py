# --------------------------------------------------------------- #
# Author: Pedro Magalhães
# Objective: Automate the dump of metadata information from a powerbi file
# Args:
# --------------------------------------------------------------- #

# Imports libraries
import sys
import pandas as pd
from sys import path
path.append("\\Program Files\\Microsoft.NET\\ADOMD.NET\\150")
from pyadomd import Pyadomd
import time

# build connection string
#pbi_dataset = "localhost:50241"
pbi_dataset = sys.argv[1]

#file_name = "ALL_LIVE"
file_name = sys.argv[2]
conn_str = 'Provider=MSOLAP; Data Source = ' + pbi_dataset

# TODO: Convert queries into a list which will be looped over to create the dumps

# Query powerbi for metadata and export as dataframe
get_table_mPartitions = """
    select * from $SYSTEM.TMSCHEMA_PARTITIONS
"""
get_calculation_items = """
    select * from $SYSTEM.TMSCHEMA_CALCULATION_ITEMS
"""
get_calculation_groups_id = """
    select * from $SYSTEM.TMSCHEMA_CALCULATION_GROUPS
"""
get_measures_dependencies = """
    select * from $SYSTEM.DISCOVER_CALC_DEPENDENCY
"""
get_measures_code = """
    select * from $SYSTEM.MDSCHEMA_MEASURES
"""

with Pyadomd(conn_str) as conn:
    with conn.cursor().execute(get_table_mPartitions) as cur:
        df = pd.DataFrame(cur.fetchone(), columns=[i.name for i in cur.description])

df.to_json(path_or_buf="C:\\Users\\Maktu\\MsC_projetos\\Upwork\\Staykeepers\\pbi_admin\\reports_dev_logs\\mPartitions_" + file_name + "_" + str(time.time()) + ".json" ,  orient="records")

with Pyadomd(conn_str) as conn:
    with conn.cursor().execute(get_calculation_items) as cur:
        df = pd.DataFrame(cur.fetchone(), columns=[i.name for i in cur.description])

df.to_json(path_or_buf="C:\\Users\\Maktu\\MsC_projetos\\Upwork\\Staykeepers\\pbi_admin\\reports_dev_logs\\calculation_itens_" + file_name + "_" + str(time.time()) + ".json" ,  orient="records")

with Pyadomd(conn_str) as conn:
    with conn.cursor().execute(get_calculation_groups_id) as cur:
        df = pd.DataFrame(cur.fetchone(), columns=[i.name for i in cur.description])

df.to_json(path_or_buf="C:\\Users\\Maktu\\MsC_projetos\\Upwork\\Staykeepers\\pbi_admin\\reports_dev_logs\\calculation_groups_" + file_name + "_" + str(time.time()) + ".json" ,  orient="records")

with Pyadomd(conn_str) as conn:
    with conn.cursor().execute(get_measures_dependencies) as cur:
        df = pd.DataFrame(cur.fetchone(), columns=[i.name for i in cur.description])

df.to_json(path_or_buf="C:\\Users\\Maktu\\MsC_projetos\\Upwork\\Staykeepers\\pbi_admin\\reports_dev_logs\\measures_dependencies_" + file_name + "_" + str(time.time()) + ".json" ,  orient="records")

with Pyadomd(conn_str) as conn:
    with conn.cursor().execute(get_measures_code) as cur:
        df = pd.DataFrame(cur.fetchone(), columns=[i.name for i in cur.description])

df.to_json(path_or_buf="C:\\Users\\Maktu\\MsC_projetos\\Upwork\\Staykeepers\\pbi_admin\\reports_dev_logs\\measures_code_" + file_name + "_" + str(time.time()) + ".json" ,  orient="records")

conn.close()
